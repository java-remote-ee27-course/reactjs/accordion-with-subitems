import { useState } from "react";

const accordionContent = [
  { text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit" },
  {
    text: "Morbi commodo, magna vel vestibulum euismod, nulla urna luctus est, at luctus eros ante et lacus.",
  },
  { text: "Sed sit amet nisl a libero vehicula varius id at ante." },
  {
    text: "Curabitur dignissim arcu felis, quis consequat enim egestas at. Suspendisse potenti. Quisque non erat enim.",
  },
];
function App() {
  const [itemOpen, setItemOpen] = useState(null);
  return (
    <div className="App">
      <h1>Accordion Demo</h1>
      <Accordion itemOpen={itemOpen} onSetItemOpen={setItemOpen} />
    </div>
  );
}

function Accordion({ itemOpen, onSetItemOpen }) {
  const [subItemOpen, setSubItemOpen] = useState(null);
  return (
    <div className="accordion">
      <AccordionItem
        id={0}
        onSetItemOpen={onSetItemOpen}
        itemOpen={itemOpen}
        text="Item 1"
      >
        <AccordionSubItem
          subId={1}
          subItemOpen={subItemOpen}
          setSubItemOpen={setSubItemOpen}
          text="SubItem 1.1"
        >
          {accordionContent[0].text}
        </AccordionSubItem>
        <AccordionSubItem
          subId={2}
          subItemOpen={subItemOpen}
          setSubItemOpen={setSubItemOpen}
          text="SubItem 1.2"
        >
          {accordionContent[1].text}
        </AccordionSubItem>
      </AccordionItem>

      <AccordionItem
        id={1}
        onSetItemOpen={onSetItemOpen}
        itemOpen={itemOpen}
        text="Item 2"
      >
        <AccordionSubItem
          subId={3}
          subItemOpen={subItemOpen}
          setSubItemOpen={setSubItemOpen}
          text="SubItem 2.1"
        >
          {accordionContent[2].text}
        </AccordionSubItem>
        <AccordionSubItem
          subId={4}
          subItemOpen={subItemOpen}
          setSubItemOpen={setSubItemOpen}
          text="SubItem 2.2"
        >
          {accordionContent[3].text}
        </AccordionSubItem>
      </AccordionItem>
    </div>
  );
}

function AccordionItem({ children, onSetItemOpen, itemOpen, id, text }) {
  //check if itemOpen has the same id as current: true / false
  const isOpen = id === itemOpen;

  function handleIsItemOpen() {
    //if is open then set current null so that on click itemOpen will not be the same as current el.id
    //if not open, open it by setting the itemOpen value to id:
    isOpen ? onSetItemOpen(null) : onSetItemOpen(id);
  }

  return (
    <div className="item">
      <div className="item-header" value={itemOpen} onClick={handleIsItemOpen}>
        {isOpen ? <span>🔺</span> : <span>🔻</span>} {text}
      </div>

      {isOpen && children}
    </div>
  );
}

function AccordionSubItem({
  children,
  text,
  subItemOpen,
  setSubItemOpen,
  subId,
}) {
  const isSubItemOpen = subItemOpen === subId;

  function handleSubItemOpen() {
    isSubItemOpen ? setSubItemOpen(null) : setSubItemOpen(subId);
  }
  return (
    <div className="sub-item" onClick={handleSubItemOpen}>
      <div className="sub-item-header">
        {isSubItemOpen ? <span>🔺</span> : <span>🔻</span>} {text}
      </div>
      <div className={`sub-item-text ${isSubItemOpen && "sub-decoration"}`}>
        {isSubItemOpen && children}
      </div>
    </div>
  );
}
export default App;
