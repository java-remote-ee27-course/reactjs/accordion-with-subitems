# Accordion Demo 2

- ReactJS app using JSX
- Accordion with closable items and subitems.
- Only one accordion item / subitem may be open at time.

## Author

Katlin

![Accordion1](./public/accordion.png)
![Accordion2](./public/accordion2.png)
![Accordion3](./public/accordion3.png)
